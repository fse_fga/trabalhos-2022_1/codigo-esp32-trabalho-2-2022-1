#ifndef SEMAFORO_H
#define SEMAFORO_H

#include <Arduino.h>
#include <string.h>
#include <ThingsBoard.h>    // ThingsBoard SDK
#include "definicoes.h"

#define TEMPORIZADOR_BOTAO 300 // milisegundos

class Semaforo{

    private:
        std::string nome;
        uint8_t id;
        gpio_pin verde;
        gpio_pin amarelo;
        gpio_pin vermelho;
        uint8_t estado;

        ThingsBoard *thingsboard;
        void atualiza_lampada(gpio_pin * lampada);
 
    public:
        Semaforo(uint8_t id, uint8_t porta_verde, uint8_t porta_amarelo, uint8_t porta_vermelho, ThingsBoard *thingsboard);
        void atualiza();
};

#endif