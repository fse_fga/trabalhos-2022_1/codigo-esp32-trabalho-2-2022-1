#include <WiFi.h>           // WiFi control for ESP32
#include <ThingsBoard.h>    // ThingsBoard SDK
#include <esp_task_wdt.h>   // Watchdog
#include "definicoes.h"
#include "semaforo.h"
#include "botao_pedestre.h"
#include "sensor_velocidade.h"

// Helper macro to calculate array size
#define COUNT_OF(x) ((sizeof(x)/sizeof(0[x])) / ((size_t)(!(sizeof(x) % sizeof(0[x])))))

// WiFi access point
#define WIFI_AP_NAME        "RPIFSE"
// WiFi password
#define WIFI_PASSWORD       "FSEFGA2022"

// See https://thingsboard.io/docs/getting-started-guides/helloworld/ 
// to understand how to obtain an access token
#if defined(PLACA_01)
  #define TOKEN               "CYGRu6QiWJ25q3ee5eoS" // ESP32 - Trabalho 1 - 01
#elif defined(PLACA_02)
  #define TOKEN               "7FtH6k6aKh3eB3Mb8vq7" // ESP32 - Trabalho 1 - 02
#elif defined(PLACA_03)
  #define TOKEN               "HSjOWbmDd0dkJuoGiyir" // ESP32 - Trabalho 1 - 02
#elif defined(PLACA_04)
  #define TOKEN               "B74VRlFQzlkuRTwZUZa1" // ESP32 - Trabalho 1 - 02
#else
  #error "Token não definido"
#endif

// ThingsBoard server instance.
#define THINGSBOARD_SERVER  "192.168.4.1"

// Baud rate for debug serial
#define SERIAL_DEBUG_BAUD    115200                    

// Initialize ThingsBoard client
WiFiClient espClient;
// Initialize ThingsBoard instance
ThingsBoard tb(espClient);
// the Wifi radio's status
int status = WL_IDLE_STATUS;

uint8_t entradas[] = {SEMAFORO_1_VERDE, 
                      SEMAFORO_1_AMARELO, 
                      SEMAFORO_1_VERMELHO, 
                      SEMAFORO_2_VERDE, 
                      SEMAFORO_2_AMARELO, 
                      SEMAFORO_2_VERMELHO};

bool entradas_estado[] = {false, false, false, false, false, false};

uint8_t saidas[] = {BOTAO_PEDESTRE_1,
                    BOTAO_PEDESTRE_2,
                    SENSOR_PASSAGEM_1,
                    SENSOR_PASSAGEM_2,
                    SENSOR_VELOCIDADE_1_A,
                    SENSOR_VELOCIDADE_1_B,
                    SENSOR_VELOCIDADE_2_A,
                    SENSOR_VELOCIDADE_2_B};

bool saidas_estado[] = {false, false, false, false, false, false, false, false};

Semaforo semaforo1(1, SEMAFORO_1_VERDE, SEMAFORO_1_AMARELO, SEMAFORO_1_VERMELHO, &tb);
Semaforo semaforo2(2, SEMAFORO_2_VERDE, SEMAFORO_2_AMARELO, SEMAFORO_2_VERMELHO, &tb);
BotaoPedestre botao1(1, BOTAO_PEDESTRE_1, &tb);
BotaoPedestre botao2(2, BOTAO_PEDESTRE_2, &tb);
SensorVelocidade sensor1(1, SENSOR_VELOCIDADE_1_A, SENSOR_VELOCIDADE_1_B, &tb);
SensorVelocidade sensor2(2, SENSOR_VELOCIDADE_2_A, SENSOR_VELOCIDADE_2_B, &tb);

// Main application loop delay
int quant = 20;

// Initial period of LED cycling.
int led_delay = 500;
// Period of sending a temperature/humidity data.
int send_delay = 500;

// Time passed after LED was turned ON, milliseconds.
int led_passed = 0;
// Time passed after temperature/humidity data was sent, milliseconds.
int send_passed = 0;

// Set to true if application is subscribed for the RPC messages.
bool subscribed = false;
// LED number that is currenlty ON.
int current_led = 0;

RPC_Response processDelayChange(const RPC_Data &data)
{
  Serial.println("Received the set delay RPC method");

  led_delay = data;

  Serial.print("Set new delay: ");
  Serial.println(led_delay);

  return RPC_Response(NULL, led_delay);
}

RPC_Response processGetDelay(const RPC_Data &data)
{
  Serial.println("Received the get value method");

  return RPC_Response(NULL, led_delay);
}

RPC_Response ligaSensorPresenca01(const RPC_Data &data)
{
  Serial.println("Liga Sensor de Presença 01");
  String msg = data.as<String>();
  Serial.println(msg);

  StaticJsonDocument<200> root;
  DeserializationError error = deserializeJson(root, msg);

  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.f_str());
  }
  int pin = root["pin"];
  bool enabled = root["enabled"];
  
  Serial.print("Pino: ");
  Serial.print(saidas[pin]);
  Serial.print("Enabled: ");
  Serial.println(enabled);

  if(pin == 0){
    botao1.aciona_botao();
  }
  else if(pin == 1){
    botao2.aciona_botao();
  }
  else if(pin == 4){
    sensor1.passa_carro();
  }
  else if(pin == 5){
    if(!sensor1.get_estado()){
      sensor1.carro_parado();
    }
    else{
      sensor1.carro_sai();
    }
  }
  else if(pin == 6){
    sensor2.passa_carro();
  }
  else if(pin == 7){
    if(!sensor2.get_estado()){
      sensor2.carro_parado();
    }
    else{
      sensor2.carro_sai();
    }
  }
  else{
    saidas_estado[pin] = !saidas_estado[pin];
    digitalWrite(saidas[pin], saidas_estado[pin]);
  }

  return RPC_Response(NULL, true);
}

// RPC handlers
RPC_Callback callbacks[] = {
  { "setValue",         processDelayChange },
  { "getValue",         processGetDelay },
  { "ligaSensorPresenca01", ligaSensorPresenca01 },
};

void InitWiFi()
{
  Serial.println("Conectando ao Wifi ...");
  // attempt to connect to WiFi network

  WiFi.begin(WIFI_AP_NAME, WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("Conectado ao Wifi");
}

void reconnect() {
  status = WiFi.status();
  if ( status != WL_CONNECTED) {
    WiFi.begin(WIFI_AP_NAME, WIFI_PASSWORD);
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
    }
    Serial.println("Conectado ao Wifi");
  }
}

// Setup inicial da aplicação
void setup() {
  Serial.begin(SERIAL_DEBUG_BAUD);
  WiFi.begin(WIFI_AP_NAME, WIFI_PASSWORD);
  InitWiFi();

  // Configura GPIO
  for (size_t i = 0; i < COUNT_OF(saidas); ++i) {
    pinMode(saidas[i], OUTPUT);
  }  
  for (size_t i = 0; i < COUNT_OF(entradas); ++i) {
    pinMode(entradas[i], INPUT);
  }

  esp_task_wdt_init(10, true);
  esp_task_wdt_add(NULL);
}

// Loop Principal
void loop() {
  esp_task_wdt_reset();
  delay(quant);

  led_passed += quant;
  send_passed += quant;

  // Reconecta ao WiFi
  if (WiFi.status() != WL_CONNECTED) {
    reconnect();
    return;
  }

  // Reconecta ao ThingsBoard
  if (!tb.connected()) {
    subscribed = false;

    // Connect to the ThingsBoard
    Serial.print("Conectando ao: ");
    Serial.print(THINGSBOARD_SERVER);
    Serial.print(" com o TOKEN ");
    Serial.println(TOKEN);
    if (!tb.connect(THINGSBOARD_SERVER, TOKEN)) {
      Serial.println("Falha de conexão");
      return;
    }
  }

  // Se inscreve no RPC
  if (!subscribed) {
    Serial.println("Cadastrando via RPC...");

    if (!tb.RPC_Subscribe(callbacks, COUNT_OF(callbacks))) {
      Serial.println("Falha ao cadastrar via RPC");
      return;
    }

    Serial.println("Cadastro pronto");
    subscribed = true;
  }

  // Check States
  semaforo1.atualiza();
  semaforo2.atualiza();
    
  tb.sendAttributeBool("sensor_passagem_1", saidas_estado[2]);     
  tb.sendAttributeBool("sensor_passagem_2", saidas_estado[3]);     

  // Process messages
  tb.loop();
}

