#ifndef BOTAO_PEDESTRE_H
#define BOTAO_PEDESTRE_H

#include <Arduino.h>
#include <string.h>
#include <ThingsBoard.h>    // ThingsBoard SDK

#define TEMPORIZADOR_BOTAO 300 // milisegundos

class BotaoPedestre{

    private:
        uint8_t id;
        std::string nome;
        uint8_t porta;
        bool estado;
        bool ultimo_estado;

        ThingsBoard *thingsboard;

        long randNumber;

    public:
        BotaoPedestre(uint8_t id, uint8_t porta, ThingsBoard *thingsboard);
        void aciona_botao();
        bool get_estado();
        void atualiza();
};

#endif