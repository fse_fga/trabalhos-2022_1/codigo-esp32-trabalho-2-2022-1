#ifndef SENSOR_VELOCIDADE_H
#define SENSOR_VELOCIDADE_H

#include <Arduino.h>
#include <string.h>
#include <ThingsBoard.h>    // ThingsBoard SDK

#define TEMPORIZADOR_MIN 30 // milisegundos VELOCIDADE de 120 Km/h
#define TEMPORIZADOR_MAX 300 // milisegundos VELOCIDADE de 12 Km/h

/* O sensor de velocidade é composto por dois sensores A e B onde o sensor A fica mais próximo do sinal de 
 * trânsico e o sensor B em seguida. A distância entre os dois sensores é de 1 metro.
 * Na passagem de um carro, o sensor B é acionado primeiro e depois o sensor A. 
 * Neste caso, para calcular a velocidade do carro passando pelos sensores, é necessário calcular o intervalo de tempo 
 * entre o acionamentdo do sensor B e do sensor A (Seja, nos dois casos, o evento de subida ou de descida) em seguida, 
 * dividir a distância entre os sensores (1 metro) pelo intervalo de tempo medido.
*/

struct sensor {
    uint8_t pin;
    char s_nome;
};

class SensorVelocidade{

    private:
        uint8_t id;
        std::string nome;
        sensor sa;
        sensor sb;

        bool estado;
        bool ultimo_estado;

        ThingsBoard *thingsboard;
        long randNumber;

        void envia_estado(sensor *s, bool estado);

    public:
        SensorVelocidade(uint8_t id, uint8_t porta_a, uint8_t porta_b, ThingsBoard *thingsboard);
        void passa_carro();
        void carro_parado();
        void carro_sai();
        bool get_estado();
};

#endif