#ifndef DEFINICOES_H
#define DEFINICOES_H

#include <stdlib.h>
#include <stdint.h>

#define PLACA_01
// #define PLACA_02
// #define PLACA_03
// #define PLACA_04


// Mapa 1
#if defined(PLACA_01) || defined(PLACA_03)
#define SEMAFORO_1_VERDE               23  // RPi GPIO 1
#define SEMAFORO_1_AMARELO             39  // RPi GPIO 26
#define SEMAFORO_1_VERMELHO            34  // RPi GPIO 21
#define SEMAFORO_2_VERDE               35  // RPi GPIO 20
#define SEMAFORO_2_AMARELO             32  // RPi GPIO 16
#define SEMAFORO_2_VERMELHO            33  // RPi GPIO 12
#define BOTAO_PEDESTRE_1               21  // RPi GPIO 8
#define BOTAO_PEDESTRE_2               22  // RPi GPIO 7
#define SENSOR_PASSAGEM_1              15  // RPi GPIO 14
#define SENSOR_PASSAGEM_2               2  // RPi GPIO 15
#define SENSOR_VELOCIDADE_1_A           4  // RPi GPIO 18
#define SENSOR_VELOCIDADE_1_B           5  // RPi GPIO 23
#define SENSOR_VELOCIDADE_2_A          18  // RPi GPIO 24
#define SENSOR_VELOCIDADE_2_B          19  // RPi GPIO 25
#else
// Mapa 2
#define SEMAFORO_1_VERDE               13  // RPi GPIO 02
#define SEMAFORO_1_AMARELO             12  // RPi GPIO 03
#define SEMAFORO_1_VERMELHO            35  // RPi GPIO 11
#define SEMAFORO_2_VERDE               34  // RPi GPIO 00
#define SEMAFORO_2_AMARELO             39  // RPi GPIO 05
#define SEMAFORO_2_VERMELHO            36  // RPi GPIO 06
#define BOTAO_PEDESTRE_1               33  // RPi GPIO 10
#define BOTAO_PEDESTRE_2               32  // RPi GPIO 09
#define SENSOR_PASSAGEM_1              14  // RPi GPIO 04
#define SENSOR_PASSAGEM_2              27  // RPi GPIO 17
#define SENSOR_VELOCIDADE_1_A          26  // RPi GPIO 27
#define SENSOR_VELOCIDADE_1_B          25  // RPi GPIO 22
#define SENSOR_VELOCIDADE_2_A          23  // RPi GPIO 13
#define SENSOR_VELOCIDADE_2_B          22  // RPi GPIO 19
#endif

struct gpio_pin{
    char *nome;
    uint8_t pin;
    bool estado;
    bool ultimo_estado;
};

#endif