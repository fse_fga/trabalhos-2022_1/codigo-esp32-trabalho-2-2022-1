#include "sensor_velocidade.h"

SensorVelocidade::SensorVelocidade(uint8_t id, uint8_t porta_a, uint8_t porta_b, ThingsBoard *thingsboard){
    this->sa.pin = porta_a;
    this->sb.pin = porta_b;
    this->sa.s_nome = 'a';
    this->sb.s_nome = 'b';
    this->thingsboard = thingsboard;
    estado = false;
    ultimo_estado = false;
    nome = "sensor_velocidade_" + std::to_string(id) + "_";
}

void SensorVelocidade::passa_carro(){
    long delay_ms = random(TEMPORIZADOR_MIN, TEMPORIZADOR_MAX);
    digitalWrite(sb.pin, false);
    envia_estado(&sb, true);
    delay(delay_ms / 2);
    digitalWrite(sb.pin, true);
    delay(delay_ms / 2);
    digitalWrite(sa.pin, false);
    envia_estado(&sa, true);
    delay(delay_ms / 2);
    digitalWrite(sa.pin, true);
    delay(500);
    envia_estado(&sa, false);
    envia_estado(&sb, false);
}

void SensorVelocidade::carro_parado(){
    estado = true;
    digitalWrite(sa.pin, false);
    envia_estado(&sa, estado);
}

void SensorVelocidade::carro_sai(){
    estado = false;
    digitalWrite(sa.pin, true);
    envia_estado(&sa, estado);
}

void SensorVelocidade::envia_estado(sensor *s, bool estado){
    digitalWrite(s->pin, true);
    std::string nome_sensor = nome + s->s_nome;
    thingsboard->sendAttributeBool(const_cast<char*>(nome_sensor.c_str()), estado);
}

bool SensorVelocidade::get_estado(){
    return estado;
}