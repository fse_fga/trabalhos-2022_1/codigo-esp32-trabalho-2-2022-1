#include "definicoes.h"
#include "botao_pedestre.h"

BotaoPedestre::BotaoPedestre(uint8_t id, uint8_t porta, ThingsBoard *thingsboard){
    this->id = id;
    this->porta = porta;
    this->thingsboard = thingsboard;
    estado = false;
    ultimo_estado = false;
    nome = "botao_pedestre_" + std::to_string(id);
}

void BotaoPedestre::aciona_botao(){
    estado = true;
    thingsboard->sendAttributeBool(const_cast<char*>(nome.c_str()), estado);
    digitalWrite(porta, true);
    delay(random(TEMPORIZADOR_BOTAO, TEMPORIZADOR_BOTAO + 100));
    digitalWrite(porta, false);
    delay(500);
    estado = false;
    thingsboard->sendAttributeBool(const_cast<char*>(nome.c_str()), estado);
}

bool BotaoPedestre::get_estado(){
    return estado;
}


