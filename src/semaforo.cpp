#include "semaforo.h"
#include <string.h>

Semaforo::Semaforo(uint8_t id, uint8_t porta_verde, uint8_t porta_amarelo, uint8_t porta_vermelho, ThingsBoard *thingsboard){
    this->verde.pin = porta_verde;
    this->amarelo.pin = porta_amarelo;
    this->vermelho.pin = porta_vermelho;
    this->id = id;
    this->thingsboard = thingsboard;
    nome = "semaforo_" + std::to_string(id) + "_";
    this->verde.nome = strcpy(new char[10], "verde");
    this->amarelo.nome = strcpy(new char[10], "amarelo");
    this->vermelho.nome = strcpy(new char[10], "vermelho");
}

void Semaforo::atualiza_lampada(gpio_pin * lampada){
    lampada->estado = (bool) digitalRead(lampada->pin);
    if(lampada->ultimo_estado != lampada->estado)
    {
        std::string nome_lampada = nome + lampada->nome;
        thingsboard->sendAttributeBool(const_cast<char*>(nome_lampada.c_str()), lampada->estado);
        lampada->ultimo_estado = lampada->estado;
    }
}

void Semaforo::atualiza(){
    atualiza_lampada(&verde);
    atualiza_lampada(&amarelo);
    atualiza_lampada(&vermelho);
}